#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Article
#------------------------------------------------------------

CREATE TABLE Article(
        Id         Int  Auto_increment  NOT NULL ,
        Titre      Varchar (50) NOT NULL ,
        Texte      Varchar (5000) NOT NULL ,
        Image      Char (50) NOT NULL ,
        Date       Date NOT NULL ,
        Like_perso Int NOT NULL
	,CONSTRAINT Article_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        id     Int  Auto_increment  NOT NULL ,
        nom    Varchar (10) NOT NULL ,
        prenom Varchar (10) NOT NULL ,
        email  Varchar (10) NOT NULL ,
        mdp    Varchar (20) NOT NULL ,
        role   Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commentaires
#------------------------------------------------------------

CREATE TABLE Commentaires(
        Id             Int  Auto_increment  NOT NULL ,
        Contenu        Text NOT NULL ,
        Date           Date NOT NULL ,
        Id_Utilisateur Int NOT NULL ,
        Id_Article     Int NOT NULL
	,CONSTRAINT Commentaires_PK PRIMARY KEY (Id)

	,CONSTRAINT Commentaires_Utilisateur_FK FOREIGN KEY (Id_Utilisateur) REFERENCES Utilisateur(Id)
	,CONSTRAINT Commentaires_Article0_FK FOREIGN KEY (Id_Article) REFERENCES Article(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: cr�e
#------------------------------------------------------------

CREATE TABLE cree(
        Id         Int NOT NULL ,
        Id_Article Int NOT NULL ,
        Date       Date NOT NULL
	,CONSTRAINT cree_PK PRIMARY KEY (Id,Id_Article)

	,CONSTRAINT cree_Utilisateur_FK FOREIGN KEY (Id) REFERENCES Utilisateur(Id)
	,CONSTRAINT cree_Article0_FK FOREIGN KEY (Id_Article) REFERENCES Article(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Modifier
#------------------------------------------------------------

CREATE TABLE Modifier(
        Id         Int NOT NULL ,
        Id_Article Int NOT NULL ,
        Date       Date NOT NULL
	,CONSTRAINT Modifier_PK PRIMARY KEY (Id,Id_Article)

	,CONSTRAINT Modifier_Utilisateur_FK FOREIGN KEY (Id) REFERENCES Utilisateur(Id)
	,CONSTRAINT Modifier_Article0_FK FOREIGN KEY (Id_Article) REFERENCES Article(Id)
)ENGINE=InnoDB;

