<?php
class Database{
  const DB_HOST = "localhost";
  const DB_BASE = "projet-linda-php";
  const DB_USER = "projet-linda-php";
  const DB_MDP = "projet-linda-php";
  const TB_USER = "utilisateur";
  const TB_MODIF = "modifier";
  const TB_CREAT = "cree";
  const TB_COMMENT = "commentaires";
  const TB_ARTICLE = "article";

  private $_BDD;
  
  public function __construct(){

    $this->connectBDD();

    }

    private function connectBDD(){
        try{
          $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_MDP, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); 
        } catch( PDOException $e){

          die("Erreur de connexion : " . $e->getMessage());
        }
      }
      Public function gerBDD(){
        return   $this-> _BDD;
      }
      public function initialisationBDD(){
        $verif = $this->_BDD->query("SHOW TABLES LIKE '".self::DB_USER ."'");
        $verif = $verif->fetchAll(PDO::FETCH_ASSOC);
          if (empty($verif) || in_array(self::DB_USER,$verif[0])){
            $sql = "CREATE TABLE IF NOT EXISTS `utilisateur` (
                `id`  NOT NULL AUTO_INCREMENT,
                `nom` varchar(10) NOT NULL,
                `prenom` varchar(10) NOT NULL,
                `email` int(11) NOT NULL,
                `mdp` float DEFAULT NULL,
                `role` float DEFAULT NULL,
                PRIMARY KEY (`Id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";
    
            $this->_BDD->query($sql);
    
            echo "La table ".self::DB_TABLE." a été créé.";
    
          }
      }
    
    }



