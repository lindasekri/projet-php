<?php
class Utilisateur {
    // les attributs de classe utilisateur.
   private $_id; 	
   private $_nom; 	
   private $_prenom;
   private $_email; 	
   private $_mdp; 	
   private $_role; 	


    // Construire l'objet
    public function __construct( Array $donnees){

        $this-> hydrate($donnees);
    }

    private function hydrate(Array $valeurs){
        foreach ($valeurs as $keys=>$valeur){
            $methode = 'set'.ucfirst($keys);
            if(method_exists($this, $methode)){
                $this->$methode($valeur);

            }
        }
    }

    public function getId(){
        return $this->_id;
      }

    public function getNom(){
        return $this->_nom;
    }

    public function getPrenom(){
        return $this->_prenom;
    }

    public function getEmail(){
        return $this->_email();
    }

    public function getMDP(){
        return $this->_mdp();
    }

    public function getRole(){
        return $this->_role;
    }
    
    protected function setId(int $id=NULL){
        if ($id === NULL) {
            $this->_id = self::$_compteur;
        } else {
            $this->_id = $id;
        }
        self::$_compteur ++;
    }  
    protected function setNom(string $nom){
        $this->_nom = $nom;

    }
    protected function setPrenom(string $prenom){
        $this->_prenom = $prenom;
    }
    protected function setEmail(string $email){
        $this->_email = $email;
    }
    protected function setMdp(string $mdp){
        $this-> _mdp = $mdp;
    }
    
    protected function setRole(string $role){
        $this-> _role = $role;
    }
}